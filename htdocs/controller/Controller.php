<?php
class Controller
{
    protected $name;

    public function __construct($name)
    {
        $this->name = $name;
    }

    /**
     * Render the view associated with an action
     */
    public function render($view)
    {
        require $viewFileName = __DIR__ . '/../views/'
                    . strtolower($this->name)
                    . '/' . strtolower($view)
                    . '.php';
    }
}