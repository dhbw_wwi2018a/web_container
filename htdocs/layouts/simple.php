<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>WWI2018a - Session 2</title>
        <style>
            /* why do we need this ??? */
            html {
                height: 100%;
                font-family: Arial;
            }

            body {
                margin: 0;
                height: 100%;
                display: grid;
                grid-template-columns: 200px 1fr;
                grid-template-rows: 60px 1fr 120px;
                /*
                    this is a comment in css
                */
                grid-template-areas:
                    "HeaderCell HeaderCell"
                    "NavigationCell ContentCell"
                    "FooterCell FooterCell"
                ;
            }

            @media screen and (max-width: 600px) {
                body {
                    grid-template-columns: 1fr;
                    grid-template-rows: 30px 1fr auto 60px;
                    grid-template-areas:
                        "HeaderCell"
                        "ContentCell"
                        "NavigationCell"
                        "FooterCell"
                    ;
                }
            }
            

            header {
                background: #ccc;
                grid-area: HeaderCell;
                box-shadow: #333 15px 0 10px;
                z-index: 1; 
            }
            nav {
                background: #ddd;
                grid-area: NavigationCell;
            }
            section {
                background: #fff;
                grid-area: ContentCell;
            }
            footer {
                color: #fff;
                background: #333;
                grid-area: FooterCell;
            }

            nav > ul {
                margin: 0;
                padding: 0;
            }
            nav > ul a {
                background: #fff;
                display: block;
                margin: 0 0 5px 0;
                padding: 10px;
                color: #000;
                text-decoration: none;
                transition: 250ms all;
            }

            nav > ul a:hover {
                background: #999;
                color: #fff;
                transform: scale(1.1);
            }
        </style>
    </head>
    <body>
        <!-- 
            this is a comment in HTML
        -->
        <header>header</header>
        <nav>
            <ul>
                <li><a href="index.php">Home</a></li>
                <li><a href="index.php?action=test">Test Action</a></li>
            </ul>
        </nav>
        <section>
        
<?php
    echo $controllerObject->render(
        strtolower($action)
    );
?>

        </section>
        <footer>footer</footer>
    </body>
</html>

