<?php
$defaultController = 'default';
$defaultAction = 'default';
$defaultLayout = 'simple';

// get controller and action from request
if (isset($_REQUEST['controller'])) {
    $controller = $_REQUEST['controller'];
} else {
    $controller = $defaultController;
}
if(isset($_REQUEST['action'])) {
    $action = $_REQUEST['action'];
} else {
    $action = $defaultAction;
}

// find controller file
$controllerFileName = __DIR__ . '/controller/'
                    . ucfirst(strtolower($controller))
                    . 'Controller.php';
require $controllerFileName;
// @TODO: handle file not found errors


// create controller class
$controllerClassName = ucfirst(strtolower($controller))
                    . 'Controller';
// @TODO: check if class exists
$controllerObject = new $controllerClassName(
    strtolower($controller)
);

// call action method in controller
$actionName = strtolower($action)
                    . 'Action';

$controllerObject->$actionName();

// render layout and view
$layout = $defaultLayout;
$layoutFileName = __DIR__ . '/layouts/'
                    . strtolower($layout)
                    . '.php';
require $layoutFileName;


// homework
/*

- url rewrite
http://localhost:8080/index.php?controller=test&action=foo

http://localhost:8080/test/foo


- add error handling


*/