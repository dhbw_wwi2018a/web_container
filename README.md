# WWI2018a - Web Development

This is a demo project for the web development course a DHBW Stuttgart.

## Prerequisites

You need to install the following software:

- docker
- docker-compose
- git
- an editor of your choice

## Installation

clone this repository with

```bash
git clone https://gitlab.com/dhbw_wwi2018a/web_container.git
```

to a location of your choice.

Then start the container with

```bash
docker-compose up
```

The website is available under http://localhost:8080

## Development

The container starts an nginx webserver and a PHP 7.3.10 runtime.

The project folder `/htdocs` is the document root for your PHP project.

## Resources

https://docs.docker.com/install/

https://docs.docker.com/compose/install/

https://git-scm.com/downloads

https://docs.nginx.com/nginx/

https://www.php.net/docs.php

https://alpinelinux.org/